import getGridDimensionsFromClientSize from "./getGridDimensions";
import Grid from "./Grid";

const dimensions = getGridDimensionsFromClientSize();

const grid = new Grid(dimensions.xNbOfBlocks, dimensions.yNbOfBlocks, dimensions.blockSegmentLength);

const button = document.getElementById('play_button') as HTMLButtonElement;

button.addEventListener('click', () => {
    if (grid.intervalId === 0) {
        grid.play();
    } else {
        grid.stop();
    }
});