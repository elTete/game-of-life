
const getGridDimensionsFromClientSize = () => {
    const blockSegmentLength = innerWidth > 600 ? 10 : 10;

    return {
        blockSegmentLength,
        xNbOfBlocks: Math.round((innerWidth * 2/3) / blockSegmentLength),
        yNbOfBlocks: Math.round((innerHeight * 2/3) / blockSegmentLength),
    }
}

export default getGridDimensionsFromClientSize;