import Block from './Block';
import IPosition from './models/IPosition';

class Grid {
    private width: number;

    private height: number;

    private segmentsLength: number;

    /**
     * mouse position in the grid so in number of blocks for x and y not in pixels
     */
    private mousePos: IPosition = {
        x: 0,
        y: 0,
    };

    private prevMousePos: IPosition = {
        x: 0,
        y: 0,
    };

    /**
     * a canvas is an html element used to draw
     * the 2d rendering context of a canvas provides a lot of methods to do so in a 2d plane
     */
    private ctx: CanvasRenderingContext2D;

    private grid: Block[][] = [];

    public intervalId = 0;

    /**
     * @param width number of blocks for the gird on x axis
     * @param height number of blocks for the grid on y axis
     * @param segmentsLength size of blocks segments in px
     */
    constructor(width: number, height: number, segmentsLength: number) {
        this.width = width;
        this.height = height;
        this.segmentsLength = segmentsLength;
        const ctx = document.querySelector('canvas')?.getContext('2d') as CanvasRenderingContext2D;

        if (!ctx) {
            throw 'cannot find canvas in html page';
        }

        this.ctx = ctx;
        this.initGrid();
    }

    /**
     * sets canvas dimensions
     * and creates a mousemove event to track mouse position in the grid
     */
    private initGrid() {
        this.ctx.canvas.width = this.width * this.segmentsLength;
        this.ctx.canvas.height = this.height * this.segmentsLength;
        this.ctx.canvas.style.backgroundColor = 'black';
        this.ctx.fillStyle = 'white';
        this.ctx.strokeStyle = 'white';

        const canvasOffsetLeft = this.ctx.canvas.offsetLeft;
        const canvasOffsetTop = this.ctx.canvas.offsetTop;

        for (let i = 0; i < this.height; i++) {
            this.grid.push([]);
            for (let j = 0; j < this.width; j++) {
                this.grid[i].push(new Block(this.ctx, j, i, this.segmentsLength));
            }
        }

        this.ctx.canvas.addEventListener('mousemove', (event) => {
            /**
             * mouse position in pixels relative to canvas position
             */
            const pos = {
                x: event.pageX - canvasOffsetLeft,
                y: event.pageY - canvasOffsetTop,
            };

            const newMousePos = {
                x: Math.floor((pos.x / (this.width * this.segmentsLength)) * this.width),
                y: Math.floor((pos.y / (this.height * this.segmentsLength)) * this.height),
            };

            if (
                newMousePos.x !== this.prevMousePos.x ||
                newMousePos.y !== this.prevMousePos.y
            ) {
                this.prevMousePos = { ...this.mousePos };
                this.mousePos = newMousePos;
                this.getHoveredBlock(true).clear(true);
                this.getHoveredBlock().fill(true);
            }
        });

        this.ctx.canvas.addEventListener('click', (event) => {
            console.log(this.mousePos);
            console.log(this.getHoveredBlock().position);
            this.getHoveredBlock().fillOrClear();
        });
    }

    public play() {
        this.intervalId = setInterval(() => {
            const analyzedGrid = this.grid.map((col) => {
                return col.map((block) => {
                    return this.analyze(block);
                });
            });
            analyzedGrid.forEach((col) => {
                col.forEach((block) => {
                    this.playBlock(block);
                });
            });
        }, 100);
    }

    public stop() {
        clearInterval(this.intervalId);
        this.intervalId = 0;

        console.log(this.grid.map(blocks => blocks.map(b => b.filled)));
    }

    private getHoveredBlock(prev = false): Block {
        if (prev) {
            return this.grid[this.prevMousePos.y][this.prevMousePos.x];
        }
        return this.grid[this.mousePos.y][this.mousePos.x];
    }

    private analyze(block: Block) {
        const getBlock = (x: number, y: number): Block | null => {
            if (x >= 0 && x < this.width && y >= 0 && y < this.height) {
                return this.grid[y][x];
            }
            return null;
        }
        const blockWithNeighbours = [
            [
                getBlock(block.position.x - 1, block.position.y - 1),
                getBlock(block.position.x - 1, block.position.y),
                getBlock(block.position.x - 1, block.position.y + 1),
            ],
            [
                getBlock(block.position.x, block.position.y - 1),
                // getBlock(block.position.x, block.position.y),
                getBlock(block.position.x, block.position.y + 1),
            ],
            [
                getBlock(block.position.x + 1, block.position.y - 1),
                getBlock(block.position.x + 1, block.position.y),
                getBlock(block.position.x + 1, block.position.y + 1),
            ],
        ];

        const filledNeighboursCount = blockWithNeighbours.reduce((acc: number, blocks) => {
            const blocksFilledInCol = blocks.reduce((acc: number, b) => {
                if (b && b.filled) {
                    return acc + 1;
                }
                return acc;
            }, 0);
            return acc + blocksFilledInCol;
        }, 0);

        return {
            block,
            neighboursCount: filledNeighboursCount,
        };
    }

    private playBlock({ block, neighboursCount }: { block: Block, neighboursCount: number }) {

        // if (block.filled) {
        //     console.log(block.position);
        //     console.log(blockWithNeighbours);
        //     console.log(blockWithNeighbours.map(bs => bs.map(b => b.position)));
        // }

        if (neighboursCount === 3) {
            block.fill();
        } else if (neighboursCount < 2 || neighboursCount > 3) {
            block.clear();
        }
    }

}

export default Grid;