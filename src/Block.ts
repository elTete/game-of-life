import IPosition from "./models/IPosition";


export default class Block {
    public filled = false;

    private hoverFilled = false;

    public position: IPosition;

    private segmentLength: number;

    private ctx: CanvasRenderingContext2D;

    /**
     * @param x x position of the block in the grid
     * @param y y position of the block in the grid
     */
    constructor(ctx: CanvasRenderingContext2D, x: number, y: number, segmentLenght: number) {
        this.position = { x: x, y: y };
        this.segmentLength = segmentLenght;
        this.ctx = ctx;
    }

    public fillOrClear() {
        if (this.filled) {
            this.clear();
        } else {
            this.fill();
        }
    }

    public fill(isHover = false) {
        const applyFill = () => {
            this.ctx.fillRect(
                this.position.x * this.segmentLength,
                this.position.y * this.segmentLength,
                this.segmentLength,
                this.segmentLength,
            );
        }
        if (!this.filled && !isHover) {
            applyFill();
            this.filled = true;
        } else if (!this.filled && !this.hoverFilled && isHover) {
            applyFill();
            this.hoverFilled = true;
        }
    }

    public clear(isHover = false) {
        const applyClear = () => {
            this.ctx.clearRect(
                this.position.x * this.segmentLength,
                this.position.y * this.segmentLength,
                this.segmentLength,
                this.segmentLength,
            );
        }
        if (this.filled && !isHover) {
            applyClear();
            this.filled = false;
        } else if (!this.filled && this.hoverFilled && isHover) {
            applyClear();
            this.hoverFilled = false;
        }
    }

}