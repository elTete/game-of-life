# README

Project asked for an interview in vanilla typescript.

Conway's game of life is a 0 player game.
It's a grid with empty (dead) and alive (filled) blocks.

Rules :
- a block has 8 neighbours (blocks in diagonal counts)
- a block with 3 alive neighbours turns alive on the next iteration
- a block with 2 alive neighbours keeps its state on the next iteration
- a block with less than 2 or more than 3 neighbours alive turns dead on the next iteration

The game is played every x ms with a setInterval function