import IPosition from "./models/IPosition";
export default class Block {
    filled: boolean;
    private hoverFilled;
    position: IPosition;
    private segmentLength;
    private ctx;
    constructor(ctx: CanvasRenderingContext2D, x: number, y: number, segmentLenght: number);
    fillOrClear(): void;
    fill(isHover?: boolean): void;
    clear(isHover?: boolean): void;
}
