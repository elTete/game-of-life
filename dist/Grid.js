"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Block_1 = require("./Block");
class Grid {
    constructor(width, height, segmentsLength) {
        var _a;
        this.mousePos = {
            x: 0,
            y: 0,
        };
        this.prevMousePos = {
            x: 0,
            y: 0,
        };
        this.grid = [];
        this.intervalId = 0;
        this.width = width;
        this.height = height;
        this.segmentsLength = segmentsLength;
        const ctx = (_a = document.querySelector('canvas')) === null || _a === void 0 ? void 0 : _a.getContext('2d');
        if (!ctx) {
            throw 'cannot find canvas in html page';
        }
        this.ctx = ctx;
        this.initGrid();
    }
    initGrid() {
        this.ctx.canvas.width = this.width * this.segmentsLength;
        this.ctx.canvas.height = this.height * this.segmentsLength;
        this.ctx.canvas.style.backgroundColor = 'black';
        this.ctx.fillStyle = 'white';
        this.ctx.strokeStyle = 'white';
        const canvasOffsetLeft = this.ctx.canvas.offsetLeft;
        const canvasOffsetTop = this.ctx.canvas.offsetTop;
        for (let i = 0; i < this.height; i++) {
            this.grid.push([]);
            for (let j = 0; j < this.width; j++) {
                this.grid[i].push(new Block_1.default(this.ctx, j, i, this.segmentsLength));
            }
        }
        this.ctx.canvas.addEventListener('mousemove', (event) => {
            const pos = {
                x: event.pageX - canvasOffsetLeft,
                y: event.pageY - canvasOffsetTop,
            };
            const newMousePos = {
                x: Math.floor((pos.x / (this.width * this.segmentsLength)) * this.width),
                y: Math.floor((pos.y / (this.height * this.segmentsLength)) * this.height),
            };
            if (newMousePos.x !== this.prevMousePos.x ||
                newMousePos.y !== this.prevMousePos.y) {
                this.prevMousePos = Object.assign({}, this.mousePos);
                this.mousePos = newMousePos;
                this.getHoveredBlock(true).clear(true);
                this.getHoveredBlock().fill(true);
            }
        });
        this.ctx.canvas.addEventListener('click', (event) => {
            console.log(this.mousePos);
            console.log(this.getHoveredBlock().position);
            this.getHoveredBlock().fillOrClear();
        });
    }
    play() {
        this.intervalId = setInterval(() => {
            const analyzedGrid = this.grid.map((col) => {
                return col.map((block) => {
                    return this.analyze(block);
                });
            });
            analyzedGrid.forEach((col) => {
                col.forEach((block) => {
                    this.playBlock(block);
                });
            });
        }, 100);
    }
    stop() {
        clearInterval(this.intervalId);
        this.intervalId = 0;
        console.log(this.grid.map(blocks => blocks.map(b => b.filled)));
    }
    getHoveredBlock(prev = false) {
        if (prev) {
            return this.grid[this.prevMousePos.y][this.prevMousePos.x];
        }
        return this.grid[this.mousePos.y][this.mousePos.x];
    }
    analyze(block) {
        const getBlock = (x, y) => {
            if (x >= 0 && x < this.width && y >= 0 && y < this.height) {
                return this.grid[y][x];
            }
            return null;
        };
        const blockWithNeighbours = [
            [
                getBlock(block.position.x - 1, block.position.y - 1),
                getBlock(block.position.x - 1, block.position.y),
                getBlock(block.position.x - 1, block.position.y + 1),
            ],
            [
                getBlock(block.position.x, block.position.y - 1),
                getBlock(block.position.x, block.position.y + 1),
            ],
            [
                getBlock(block.position.x + 1, block.position.y - 1),
                getBlock(block.position.x + 1, block.position.y),
                getBlock(block.position.x + 1, block.position.y + 1),
            ],
        ];
        const filledNeighboursCount = blockWithNeighbours.reduce((acc, blocks) => {
            const blocksFilledInCol = blocks.reduce((acc, b) => {
                if (b && b.filled) {
                    return acc + 1;
                }
                return acc;
            }, 0);
            return acc + blocksFilledInCol;
        }, 0);
        return {
            block,
            neighboursCount: filledNeighboursCount,
        };
    }
    playBlock({ block, neighboursCount }) {
        if (neighboursCount === 3) {
            block.fill();
        }
        else if (neighboursCount < 2 || neighboursCount > 3) {
            block.clear();
        }
    }
}
exports.default = Grid;
//# sourceMappingURL=Grid.js.map