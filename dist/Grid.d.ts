declare class Grid {
    private width;
    private height;
    private segmentsLength;
    private mousePos;
    private prevMousePos;
    private ctx;
    private grid;
    intervalId: number;
    constructor(width: number, height: number, segmentsLength: number);
    private initGrid;
    play(): void;
    stop(): void;
    private getHoveredBlock;
    private analyze;
    private playBlock;
}
export default Grid;
