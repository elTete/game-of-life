"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const getGridDimensions_1 = require("./getGridDimensions");
const Grid_1 = require("./Grid");
const dimensions = (0, getGridDimensions_1.default)();
const grid = new Grid_1.default(dimensions.xNbOfBlocks, dimensions.yNbOfBlocks, dimensions.blockSegmentLength);
const button = document.getElementById('play_button');
button.addEventListener('click', () => {
    if (grid.intervalId === 0) {
        grid.play();
    }
    else {
        grid.stop();
    }
});
//# sourceMappingURL=index.js.map