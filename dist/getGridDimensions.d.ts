declare const getGridDimensionsFromClientSize: () => {
    blockSegmentLength: number;
    xNbOfBlocks: number;
    yNbOfBlocks: number;
};
export default getGridDimensionsFromClientSize;
