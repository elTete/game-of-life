"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Block {
    constructor(ctx, x, y, segmentLenght) {
        this.filled = false;
        this.hoverFilled = false;
        this.position = { x: x, y: y };
        this.segmentLength = segmentLenght;
        this.ctx = ctx;
    }
    fillOrClear() {
        if (this.filled) {
            this.clear();
        }
        else {
            this.fill();
        }
    }
    fill(isHover = false) {
        const applyFill = () => {
            this.ctx.fillRect(this.position.x * this.segmentLength, this.position.y * this.segmentLength, this.segmentLength, this.segmentLength);
        };
        if (!this.filled && !isHover) {
            applyFill();
            this.filled = true;
        }
        else if (!this.filled && !this.hoverFilled && isHover) {
            applyFill();
            this.hoverFilled = true;
        }
    }
    clear(isHover = false) {
        const applyClear = () => {
            this.ctx.clearRect(this.position.x * this.segmentLength, this.position.y * this.segmentLength, this.segmentLength, this.segmentLength);
        };
        if (this.filled && !isHover) {
            applyClear();
            this.filled = false;
        }
        else if (!this.filled && this.hoverFilled && isHover) {
            applyClear();
            this.hoverFilled = false;
        }
    }
}
exports.default = Block;
//# sourceMappingURL=Block.js.map